# Angular - The Complete Guide (Maximilian Schwarzmüller)

## Sections with Branches (github)
* [Section 1 - Getting Started](https://github.com/willian-fer/angular-udemy/tree/SECTION1)
* [Section 2 - The Basics](https://github.com/willian-fer/angular-udemy/tree/SECTION2)
* [Section 3 - Course Project - The Basics](https://github.com/willian-fer/angular-udemy/tree/SECTION3)
* [Section 4 - Debugging](#)
* [Section 5 - Components & Databinding Deep Dive](https://github.com/willian-fer/angular-udemy/tree/SECTION5)
* [Section 6 - Course Project - Components & Databinding](https://github.com/willian-fer/angular-udemy/tree/SECTION6)
* [Section 7 - Directives Deep Dive](https://github.com/willian-fer/angular-udemy/tree/SECTION7)
* [Section 8 - Course Project - Directives](https://github.com/willian-fer/angular-udemy/tree/SECTION8)
* [Section 9 - Using Services & Dependency Injection](https://github.com/willian-fer/angular-udemy/tree/SECTION9) 
* [Section 10 - Course Project - Services & Dependency Injection](https://github.com/willian-fer/angular-udemy/tree/SECTION10)
* [Section 11 - Changing Pages with Routing](https://github.com/willian-fer/angular-udemy/tree/SECTION11)
* [Section 12 - Course Project - Routing](https://github.com/willian-fer/angular-udemy/tree/SECTION12)
* [Section 13 - Understanding Observables](https://github.com/willian-fer/angular-udemy/tree/SECTION13)
* [Section 14 - Course Project - Observables](https://github.com/willian-fer/angular-udemy/tree/SECTION14)
* [Section 15 - Handling Forms in Angular Apps](https://github.com/willian-fer/angular-udemy/tree/SECTION15)
* [Section 16 - Course Project - Forms](https://github.com/willian-fer/angular-udemy/tree/SECTION16)
* [Section 17 - Using Pipes to Transform Output](https://github.com/willian-fer/angular-udemy/tree/SECTION17)
* [Section 18 - Making Http Requests](https://github.com/willian-fer/angular-udemy/tree/SECTION18)
* [Section 19 - Course Project - Http](https://github.com/willian-fer/angular-udemy/tree/SECTION19)
* [Section 20 - Authentication & Route Protection in Angular Apps](https://github.com/willian-fer/angular-udemy/tree/SECTION20)
* [Section 21 - Using Angular Modules & Optimizing Apps](https://github.com/willian-fer/angular-udemy/tree/SECTION21) 
* [Section 22 - Deployng an Angular App](https://github.com/willian-fer/angular-udemy/tree/SECTION22) 
* [Section 23 - Bonus: The HttpClient](https://github.com/willian-fer/angular-udemy/tree/SECTION23) 
* [Section 24 - Bonus: Working with NgRx in our Project](https://github.com/willian-fer/angular-udemy/tree/SECTION24) 
* [Section 25 - Bonus: Angular Universal](https://github.com/willian-fer/angular-udemy/tree/SECTION25) 
* [Section 26 - Angular Animation](https://github.com/willian-fer/angular-udemy/tree/SECTION26) 
* [Section 27 - Adding Offline Capabilities with Service Workers](https://github.com/willian-fer/angular-udemy/tree/SECTION27) 
* [Section 28 - A Basic Introduction to Unit Testing in Angular Apps](https://github.com/willian-fer/angular-udemy/tree/SECTION28) 
* [Section 29 - Course Roundup](https://github.com/willian-fer/angular-udemy/tree/SECTION29) 
* [Section 30 - Angular 6 Changes & new Features](https://github.com/willian-fer/angular-udemy/tree/SECTION30) 
* [Section 31 - Custom Project & Workflow Setup](https://github.com/willian-fer/angular-udemy/tree/SECTION31) 
* [Section 32 - Bonus: TypeScript Introduction (for Angular 2 Usage)](https://github.com/willian-fer/angular-udemy/tree/SECTION32) 

